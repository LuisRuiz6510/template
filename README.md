# Primeros pasos con la base de proyectos

Este proyecto fue iniciado con [Create React App](https://github.com/facebook/create-react-app).

## Scripts disponibles

En el directorio del proyecto, puede ejecutar:

### `npm start`

Ejecuta la aplicación en el modo de desarrollo.\
Abrir [http://localhost:3000](http://localhost:3000) para verlo en el navegador.

### `npm run build`

Construye la aplicación para producción en la carpeta `build`.\
Empaqueta correctamente React en modo de producción y optimiza la compilación para obtener el mejor rendimiento.

### `npm run format` y `npm run lint`

**Note: Ejecutar uno a uno y solucionar los errores mostrados.**

Comandos para formatear y dar los estilos necesarios del código.
